package com.example.fragments_styles_views_threads

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.fragments_styles_views_threads.databinding.PostItemBinding

class PostAdapter : RecyclerView.Adapter<PostAdapter.PostHolder>() {
    val postList = ArrayList<Post>()

    class PostHolder(item: View) : RecyclerView.ViewHolder(item) {
        val binding = PostItemBinding.bind(item)
        @RequiresApi(Build.VERSION_CODES.M)
        fun bind(post: Post) = with(binding) {
            tvPostText.text = post.text
            tvPostText.setTextAppearance( post.style )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.post_item, parent, false)
        return PostHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        holder.bind(postList[position])
    }

    override fun getItemCount(): Int {
        return postList.size
    }

    fun addPost(post: Post) {
        postList.add(post)
        notifyDataSetChanged()
    }

    fun changePostStyle(id: Int, style: Int){
        if (id >= postList.size) return

        postList[id].style = style
        notifyDataSetChanged()
    }

}