package com.example.fragments_styles_views_threads

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.fragments_styles_views_threads.databinding.FragmentProfileBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private var mAuth: FirebaseAuth? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(layoutInflater)

        mAuth = FirebaseAuth.getInstance()

        val mUser = mAuth!!.currentUser
        val mDatabaseReference = FirebaseDatabase.getInstance().reference.child("users")
        val mUserReference = mDatabaseReference.child(mUser!!.uid)

        binding.tvEmail.text = mUser.email

        mUserReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                binding.tvNickname1.text = snapshot.child("nickname").value as String
                binding.tvBday.text = snapshot.child("birthday").value as String
            }

            override fun onCancelled(error: DatabaseError) {}
        })

        binding.btnLogout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            Navigation.findNavController(binding.root).navigate(R.id.fromProfileToLogin)
        }

        return binding.root
    }

    companion object {

        fun newInstance() =
            ProfileFragment()
    }
}