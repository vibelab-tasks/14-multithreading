package com.example.fragments_styles_views_threads

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.example.fragments_styles_views_threads.databinding.FragmentRegistrationBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


class RegistrationFragment : Fragment() {

    private lateinit var binding: FragmentRegistrationBinding
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentRegistrationBinding.inflate(layoutInflater)
        val view = binding.root

        binding.btnBackToLogin.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.navigateToLoginFromRegistration)
        }

        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference.child("users")
        mAuth = FirebaseAuth.getInstance()

        binding.btnRegistration.setOnClickListener { createNewAccount() }

        return view
    }

    private fun createNewAccount() = with(binding) {
        val nickname = editTextPersonName.text.toString()
        val email = editTextEmailAddress.text.toString()
        val password = editTextPassword.text.toString()
        val birthday = editTextDate.text.toString()

        if (nickname == "" || email == "" || password == "" || birthday == "") {
            Toast.makeText(context, "Enter all data!", Toast.LENGTH_LONG)
        }
        else {
            mAuth!!
                .createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val userId = mAuth!!.currentUser!!.uid
                        val curUserDB = mDatabaseReference!!.child(userId)
                        curUserDB.child("nickname").setValue(nickname)
                        curUserDB.child("birthday").setValue(birthday)
                        Toast.makeText(activity, "Registration completed!", Toast.LENGTH_SHORT)
                    } else {
                        println("problem ${task.exception}")
                    }
                }
        }
    }

    companion object {
        fun newInstance() =
            RegistrationFragment()
    }
}