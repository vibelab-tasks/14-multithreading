package com.example.fragments_styles_views_threads

import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.navigation.Navigation
import com.example.fragments_styles_views_threads.databinding.FragmentLoginBinding
import com.google.firebase.auth.FirebaseAuth


class LoginFragment : Fragment() {

    lateinit var bottomBarEnabler: BottomBarEnabler
    private var mAuth: FirebaseAuth? = null
    private lateinit var binding: FragmentLoginBinding


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BottomBarEnabler) {
            bottomBarEnabler = context
        } else {
            throw RuntimeException("Context must be inherited from interface BottomBarEnabler")
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bottomBarEnabler.setNavbarVisibility(false)

        binding = FragmentLoginBinding.inflate(layoutInflater)
        val view = binding.root


        binding.btnSignUp.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.navigateToRegistrationFragment)
        }

        mAuth = FirebaseAuth.getInstance()

        binding.btnLogin.setOnClickListener {
            loginUser(view)
        }

        return view
    }

    private fun loginUser(view: View) {

        val email = binding.editTextTextEmailAddress.text.toString().trim()
        val password = binding.editTextTextPassword.text.toString().trim()

        if (email == "" || password == "") {
            Toast.makeText(context, "Enter email and password", Toast.LENGTH_SHORT)
            return
        }

        mAuth!!.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                println("login event [$email] [$password]")
                if (task.isSuccessful) {
                    println("login event success")
                    Navigation.findNavController(view).navigate(R.id.fromLoginToProfile)
                    bottomBarEnabler.setNavbarVisibility(true)

                } else {
                    Toast.makeText(activity, "Some problem occurred", Toast.LENGTH_LONG)
                    println("problem ${task.exception}")
                }
            }
    }

    companion object {

        fun newInstance() = LoginFragment()

    }
}